FROM image-mirror-prod-registry.cloud.duke.edu/library/ruby:3.1
ENV HOME=/opt/app-root

RUN apt-get update -qq \
      && apt-get upgrade -y \
      && apt-get install -y --no-install-recommends \
        zlib1g-dev \
        build-essential \
        libcurl4-openssl-dev \
      && rm -rf /var/lib/apt/lists/* /tmp/* /var/tmp/* /var/cache/apt/archives

ENV APP_PATH /opt/app-root/src

WORKDIR ${APP_PATH}

COPY Gemfile* ${APP_PATH}/
RUN bundle install --retry 3 \
      && rm -rf `gem env gemdir`/cache/*.gem \
      && find `gem env gemdir`/gems/ -name "*.c" -delete \
      && find `gem env gemdir`/gems/ -name "*.o" -delete

# Copy the application into the container
COPY . $APP_PATH

RUN chgrp -R root $(gem env gemdir) /usr/local/lib/ruby \
    && chmod -R g=rwX /opt/app-root $(gem env gemdir) /usr/local/lib/ruby
