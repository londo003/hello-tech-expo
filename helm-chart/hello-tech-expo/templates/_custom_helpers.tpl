{{/* vim: set filetype=mustache: */}}
{{- define "project.imagePath" }}
{{- if .Values.image.full }}
{{- .Values.image.full }}
{{- else }}
{{- printf "%s:%s" .Values.image.repository .Values.image.tag }}
{{- end }}
{{- end }}