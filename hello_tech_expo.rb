#!/usr/local/bin/ruby
require 'sinatra'
set :bind, '0.0.0.0'
get '/' do
  "Hello Tech Expo from #{ENV['DEPLOYED_ENVIRONMENT']} WOOT!"
end
